package main

import "fmt"
import "os"
import "net/http"

func main() {

	exibeNomes()

	for {
		exibeMenu()
		comando := leComando()

		switch comando {
		case 1:
			iniciarMonitoramento()
		case 2:
			fmt.Println("Exibindo Logs...")
		case 0:
			fmt.Println("Saindo do programa")
			os.Exit(0)
		default:
			fmt.Println("Não conheço este comando")
			os.Exit(-1)
		}
	}
}

func iniciarMonitoramento() {
	fmt.Println(" ------------------iniciarMonitoramento------------------------")
	fmt.Println("Monitorando...")

	var sites [4]string
	sites[0] = "https://globo.com/"
	sites[1] = "https://www.amozon.com.br"
	sites[2] = "https://www.google.com.br"

	site := "https://www.tecban.com.br/para-os-bancos"
	resp, _ := http.Get(site)
	fmt.Println(resp)

	if resp.StatusCode == 200 {
		fmt.Println("Site:", site, "foi carregado com sucesso!")
	} else {
		fmt.Println("Site:", site, "está com problemas. Status Code:", resp.StatusCode)
	}
	fmt.Println(" ------------------Fim Monitorando------------------------")
}

func exibeNomes() {

	fmt.Println(" ------------------exibeNomes------------------------")

	nomes := []string{"Samuel", "Daniel", "Ana"}

	for i := 0; i < len(nomes); i++ {
		fmt.Println(nomes[i])
	}

	for i, nomes := range nomes {
		fmt.Println("Estou passando na posição", i,
			"do meu slice e essa posição tem o site", nomes)
	}
	fmt.Println("O meu slice tem", len(nomes), "itens")

	fmt.Println(" ------------------fim exibeNomes-----------------------")
}

func exibeMenu() {
	fmt.Println(" -------------------exibeMenu-----------------------")
	fmt.Println("1- Iniciar Monitoramento")
	fmt.Println("0- Sair do Programa")
	fmt.Println(" -------------------Escoher o comando -----------------------")
}

func leComando() int {
	var comandoLido int
	fmt.Scan(&comandoLido)
	fmt.Println("O comando escolhido foi", comandoLido)

	return comandoLido
}
